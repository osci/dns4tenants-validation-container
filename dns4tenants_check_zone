#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# --
# dns4tenants_check_zone, check zone for tenants CI
# Copyright (C) 2023  Marc Dequènes (Duck)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ++

import argparse
import sys
from pathlib import Path
import io
import subprocess
import re
import dns.resolver


VERSION = "0.0.1"

zone_path_regex = r"^(?P<zone>[a-zA-Z0-9.-]+)\.zone$"
checkzone_serial_regex = r"loaded serial\s+(?P<serial>\d+)$"

# Parse command line arguments

parser = argparse.ArgumentParser(description="Update Distributed DNS Zones")
parser.add_argument(
    "--version", action="version", version="%(prog)s {}".format(VERSION)
)
parser.add_argument(
    "--debug", action="store_true", help="display debug messages and do not send mails"
)
parser.add_argument("zone_path", help="zone file path")
parser.add_argument(
    "--check",
    action="store_true",
    help="check if zone serial matches DNS instead of expecting a zone change",
)
parser.add_argument(
    "--new", action="store_true", help="is it a new zone? if so skip serial checks"
)
args = parser.parse_args()

zone_path = Path(args.zone_path)
if not zone_path.exists():
    print(f"Zone file {zone_path} does not exist")
    sys.exit(1)

m = re.search(zone_path_regex, zone_path.name)
if not m:
    print(f"Zone file {zone_path} is not corectly formated (<zone>.zone)")
    sys.exit(1)
zone = m.group("zone")


# Check new zone syntax and get serial

r = subprocess.run(
    [
        "named-checkzone",
        "-i",
        "full",
        "-k",
        "fail",
        "-M",
        "fail",
        "-n",
        "fail",
        "-S",
        "fail",
        zone,
        zone_path,
    ],
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE,
    encoding="utf-8",
)
if r.returncode != 0:
    print(f"Zone '{zone}' validation failed:\n{r.stdout} {r.stderr}")
    sys.exit(2)
print(f"Zone '{zone}' is valid")

m = re.search(checkzone_serial_regex, r.stdout, re.MULTILINE)
if not m:
    print(f"Zone '{zone}' serial lookup failed")
    sys.exit(2)
new_serial = int(m.group("serial"))
print(f"Zone '{zone}' new serial: {new_serial}")

if args.new:
    print(f"Zone '{zone}' is new, skipping serial checks")
else:
    try:
        answer = dns.resolver.resolve(zone, "SOA")
    except dns.resolver.NXDOMAIN:
        print(f"Zone '{zone}' current serial cannot be fetched (DNS problem)")
        sys.exit(1)
    cur_serial = answer.rrset[0].serial
    print(f"Zone '{zone}' current serial: {cur_serial}")

    if args.check:
        if new_serial == cur_serial:
            print(f"Zone '{zone}' serial matches DNS")
        else:
            print(f"Zone '{zone}' serial does not match DNS")
            sys.exit(2)

    else:
        # Compare serial and check if changes and valid bump

        # includes serial reset support:
        # you might need to change the numbering scheme or wish to lower the serial.
        # the serial is a 32-bit unsigned integer that loops when you increase it too much.
        # to lower the number without waiting you need to increase the serial by steps until you loop and reach the desired number; without steps the DNS secondaries will reject the zone transfer

        if new_serial == cur_serial:
            print(f"Zone '{zone}' serial not updated")
            sys.exit(2)
        if new_serial > cur_serial:
            dist = new_serial - cur_serial
        # also handle serial reset situation
        elif new_serial < cur_serial:
            dist = 4294967295 - cur_serial + new_serial
        # big steps are not allowed by DNS softwares, to reset you need to do it in steps
        if dist > 2147483647:
            print(f"Zone '{zone}' serial increased too fast (or decreased)")
            sys.exit(2)

print(f"Zone '{zone}' is OK")
sys.exit(0)
