# DNS4Tenants Validation Container

## Introduction

dns4tenants-validation-container is a container aimed at checking DNS zones in a Gitlab or Github CI. It is made to fulfill the requirements for the dns4tenants system but could be used separately.

## Usage

Using this container with Gitlab CI is direct with a small `.gitlab-ci.yml` configuration:

```
variables:
  GIT_STRATEGY: clone

check-zones:
  stage: test
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  image:
    name: registry.gitlab.com/osci/dns4tenants-validation-container
  script: /usr/local/bin/dns4tenants_check_all_zones.sh
```

Using this container with Github CI uses an [intermediate container](https://github.com/OSAS/dns4tenants-validation) which transforms this one into a Github Action and can be used with a simple workflow:

```
---
name: Zones check

on:
  workflow_dispatch:
  push:
    branches:
      - main
  pull_request:
    branches:
      - '**'
  schedule:
    - cron: "42 3 * * 1"

jobs:
  build:
    runs-on: ubuntu-latest
    steps:
      # first load the base branch
      - uses: actions/checkout@v4
        with:
          ref: ${{ github.event.pull_request.head.sha }}
          fetch-depth: 0
      # and then the PR commit
      - uses: actions/checkout@v4
      - uses: OSAS/dns4tenants-validation@main
```

## Checks of the zones

The `dns4tenants_check_all_zones.sh` script detects the CI type (Gitlab or Github) and if this is a PR or a run on a branch.

If this is a PR then it extracts the list of changes zones and validate them using the `dns4tenants_check_zone` script.

If not a PR then it validates all the zones using the same script.

The validation script checks if the zone is valid first. If this is a PR then the serial will have to be bumped, else the serial would have to match the current one in the DNS.
