#!/bin/bash
set -eux

# do not stop at the first error, so that reports for all changes
# are displayed
RET=0

mkdir -p /etc/named/masters
[ -d placeholders ] && cp -a placeholders/. /etc/named/masters/

# List of variables
export

OPTS=

# only check modified files if a PR
#
# Gitlab (merge_requests only mode)
if [ -n "${CI_MERGE_REQUEST_DIFF_BASE_SHA:=}" ]; then
  files=$(git diff --name-only "${CI_MERGE_REQUEST_DIFF_BASE_SHA}"..."$CI_COMMIT_SHA")
  base_sha=$CI_MERGE_REQUEST_DIFF_BASE_SHA
#
# GitHub (PR)
elif [ "${GITHUB_EVENT_NAME:=}" = "pull_request" ]; then
  # workaround for https://github.com/actions/checkout/issues/363
  git config --global --add safe.directory "$GITHUB_WORKSPACE"
  files=$(git diff --name-only origin/${GITHUB_BASE_REF}...origin/${GITHUB_HEAD_REF})
  base_sha=origin/$GITHUB_BASE_REF
#
# not a PR, checking all files
else
  # if this is a post-merged PR check then we need to give time to propagate
  sleep 10m

  files=$(find . -name "*.zone")
  OPTS=--check
fi

for f in $(echo "$files" | grep '\.zone$'); do
  NEW=
  if [ -n "${base_sha:=}" ] && ! git cat-file -e "${base_sha}:${f}" 2> /dev/null; then
    NEW=--new
  fi
  if ! /usr/local/bin/dns4tenants_check_zone $f $OPTS $NEW; then
    RET=1
  fi
done

exit $RET

